package businessLayer;

public class Client{
private int ID;
private String Nume;

private String Telefon;
private String Adresa;

public Client(int ID,String Nume,String Telefon,String Adresa){
	this.ID=ID;
	this.Nume=Nume;
	this.Telefon=Telefon;
	this.Adresa=Adresa;
}

public int getId(){
	return ID;
}
public void setId(int ID){
	this.ID=ID;
}
public String getNume(){
	return Nume;
}
public void setNume(String Nume){
	this.Nume=Nume;
}
public String getTelefon(){
	return Telefon;
}
public void setTelefon(String Telefon){
	this.Telefon=Telefon;
}
public String getAdresa(){
	return Adresa;
}
public void setAdresa(String Adresa){
	this.Adresa=Adresa;
}
@Override
public String toString(){
	return String.format("Client [ID=%s, Nume=%s, Telefon=%s, Adresa=%s]", ID,Nume,Telefon,Adresa);
}
}



