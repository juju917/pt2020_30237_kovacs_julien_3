package businessLayer;

public class Comanda {
	private int ID;
	private int TOTAL;
	private int ID_PRODUS;
	private int ID_CLIENT;
	private int CANTITATE;
	private String DATA;
	
	public Comanda(int ID,int ID_PRODUS,int ID_CLIENT,int CANTITATE,int TOTAL,String DATA){
		this.ID=ID;
		this.TOTAL=TOTAL;
		this.ID_PRODUS=ID_PRODUS;
		this.ID_CLIENT=ID_CLIENT;
		this.CANTITATE=CANTITATE;
		this.DATA=DATA;
	}
	
	public int getId(){
		return ID;
	}
	public void setId(int ID){
		this.ID=ID;
	}
	public int getID_PRODUS(){
		return ID_PRODUS;
	}
	public void setID_PRODUS(int ID_PRODUS){
		this.ID_PRODUS=ID_PRODUS;
	}
	public int getID_CLIENT(){
		return ID_CLIENT;
	}
	public void setID_CLIENT(int ID_CLIENT){
		this.ID_CLIENT=ID_CLIENT;
	}
	public int getTOTAL(){
		return TOTAL;
	}
	public void setTOTAL(int TOTAL){
		this.TOTAL=TOTAL;
	}
	public int getCANTITATE(){
		return CANTITATE;
	}
	public void setCANTITATE(int CANTITATE){
		this.CANTITATE=CANTITATE;
	}
	public String getDATA(){
		return DATA; 
	}
	public void setDATA(String DATA){
		this.DATA=DATA;
	}
	@Override
	public String toString(){
		return String.format("Comanda [ID=%s, ID_PRODUS=%s, ID_CLIENT=%s, CANTITATE=%s, TOTAL=%s, DATA=%s]", ID,ID_PRODUS,ID_CLIENT,CANTITATE,TOTAL,DATA);
	}

}

