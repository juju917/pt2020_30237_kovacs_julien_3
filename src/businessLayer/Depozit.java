package businessLayer;

public class Depozit {
	private int ID;
	private int ID_PRODUS;
	private int CANTITATE;
	
	public Depozit(int ID_PRODUS, int CANTITATE){
		
		this.ID_PRODUS=ID_PRODUS;
		this.CANTITATE=CANTITATE;
	}
	
	public int getId(){
		return ID;
	}
	public void setId(int id){
		this.ID=id;
	}
	public int getID_PRODUS(){
		return ID_PRODUS;
	}
	public void setID_PRODUS(int ID_PRODUS){
		this.ID_PRODUS=ID_PRODUS;
	}
	public int getCANTITATE(){
		return CANTITATE;
	}
	public void setCANTITATE(int CANTITATE){
		this.CANTITATE=CANTITATE;
	}
	@Override
	public String toString(){
		return String.format("Depozit [ID=%s, ID_PRODUS=%s]", ID,ID_PRODUS);
	}
}
