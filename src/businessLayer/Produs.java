package businessLayer;

public class Produs {
	private int id_produs;
	private String Nume_produs;
	private int Cost;
	
	public Produs(int id_produs,String Nume_produs,int Cost){
		this.id_produs=id_produs;
		this.Nume_produs=Nume_produs;
		this.Cost=Cost;
	}
	public int getId(){
		return id_produs;
	}
	public void setId(int id_produs){
		this.id_produs=id_produs;
	}
	public String getNume_produs(){
		return Nume_produs;
	}
	public void setNume_produs(String Nume_produs){
		this.Nume_produs=Nume_produs;
	}
	public int getCost(){
		return Cost;
	}
	public void setCost(int Cost){
		this.Cost=Cost;
	}
	@Override
	public String toString(){
		return String.format("Produse [id_produs=%s, Nume_produs=%s, Cost=%s]", id_produs,Nume_produs,Cost);
	}
}

