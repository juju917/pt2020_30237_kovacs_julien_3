package dataAccessLayer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

import businessLayer.Client;

public class ClientAdministration {
	public  Connection con=null;
	
	public ClientAdministration() throws SQLException, ClassNotFoundException{
		Class.forName("com.mysql.jdbc.Driver");
		con = DriverManager.getConnection("jdbc:mysql://localhost:3306/manage","root","Anonimus1.");
	
	}

	public List<Client> refresh() throws SQLException{
		List<Client> lista = new ArrayList<Client>();
		Statement statement = (Statement) con.createStatement();
		ResultSet result = statement.executeQuery("select * from client");
		
		while(result.next()){
			Client temp = convertRowToClient(result);
			lista.add(temp);
		}
		return lista;
	}
	
	public void insertClient(String Nume ,String Telefon, String Adresa) throws SQLException{
		String s1 = "INSERT INTO client (Nume,Telefon,Adresa) VALUES (?,?,?)";
		PreparedStatement st = (PreparedStatement) con.prepareStatement(s1);
		st.setString(1, Nume);
		st.setString(2, Telefon);
		st.setString(3, Adresa);
		st.executeUpdate();
		st.close();
	}
	
	public void deleteClientID(int ID) throws SQLException{
		PreparedStatement st = (PreparedStatement) con.prepareStatement("delete from client where ID=?");
		st.setInt(1, ID);
		st.executeUpdate();
		st.close();
	}
	public void deleteClientNume(String Nume) throws SQLException{
		PreparedStatement st = (PreparedStatement) con.prepareStatement("delete from client where Nume='"+Nume+"'");
		st.executeUpdate();
		st.close();
	}
	public void updateClient(int ID,String Nume,String Telefon, String Adresa) throws SQLException{
		PreparedStatement st = (PreparedStatement) con.prepareStatement("update client set Nume='"+Nume+"',Telefon='"+Telefon+"',Adresa='"+Adresa+"' where ID='"+ID+"'");
	    st.executeUpdate();
		st.close();
	}
	
	
private Client convertRowToClient(ResultSet myRs) throws SQLException {
		
		int ID = myRs.getInt("ID");
		String Nume = myRs.getString("Nume");
		String Telefon = myRs.getString("Telefon");
		String Adresa = myRs.getString("Adresa");
		
		Client temp = new Client(ID,Nume,Telefon,Adresa);
		
		return temp;
	}
}
