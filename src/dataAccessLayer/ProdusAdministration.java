package dataAccessLayer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

import businessLayer.Produs;

public class ProdusAdministration {
	private Connection con;
	
	public ProdusAdministration() throws SQLException{
		con = DriverManager.getConnection("jdbc:mysql://localhost:3306/manage","root","Anonimus1.");
	}
	
	public List<Produs> toateProdusele() throws SQLException{
		List<Produs> lista = new ArrayList<Produs>();
		Statement statement = (Statement) con.createStatement();
		ResultSet result = statement.executeQuery("select * from produs");
		
		while(result.next()){
			Produs temp = convertRowToProdus(result);
			lista.add(temp);
		}
		return lista;
	}
	
	public void insertProdus(int id_produs,String Nume_produs, int Cost) throws SQLException{
		String s1 = "insert into produs (id_produs,Nume_produs,Cost) values (?,?,?)";
		PreparedStatement st = (PreparedStatement) con.prepareStatement(s1);
		st.setInt(1, id_produs );
		st.setString(2, Nume_produs);
		st.setInt(3, Cost);
		st.executeUpdate();
		st.close();
		
	}
	
	public void deleteProdusID(int id_produs) throws SQLException{
		PreparedStatement st = (PreparedStatement) con.prepareStatement("delete from produs where id_produs='"+id_produs+"'");
		st.executeUpdate();
		st.close();
	}
	public void deleteProdusNume(String Nume_produs) throws SQLException{
		PreparedStatement st = (PreparedStatement) con.prepareStatement("delete from produs where Nume_produs='"+Nume_produs+"'");
		st.executeUpdate();
		st.close();
	}
	public void updateProdus(int id_produs,String Nume_produs,int Cost) throws SQLException{
		PreparedStatement st = (PreparedStatement) con.prepareStatement("update produs set Nume_produs='"+Nume_produs+"',Cost='"+Cost+"' where id_produs='"+id_produs+"'");
		st.executeUpdate();
		st.close();
	}
	public List<Produs> searchProdus(String Nume_produs) throws SQLException{
		List<Produs> lista = new ArrayList<Produs>();
		Nume_produs += "%";
		PreparedStatement statement = (PreparedStatement) con.prepareStatement("select * from produs where Nume_produs like ?");
		statement.setString(1,Nume_produs);
		ResultSet result = statement.executeQuery();
		
		while(result.next()){
			Produs temp = convertRowToProdus(result);
			lista.add(temp);
		}
		return lista;
	}
	
	
	
	private Produs convertRowToProdus(ResultSet myRs) throws SQLException {
		
		int id_produs = myRs.getInt("id_produs");
		String Nume_produs = myRs.getString("Nume_produs");
		int Cost = myRs.getInt("Cost");
		
		Produs temp = new Produs(id_produs,Nume_produs,Cost);
		
		return temp;
	}
}
