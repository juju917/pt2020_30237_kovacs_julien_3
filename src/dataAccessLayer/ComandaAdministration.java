package dataAccessLayer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

import businessLayer.Comanda;

public class ComandaAdministration {
	private Connection con;
	public ComandaAdministration() throws SQLException{
		con = DriverManager.getConnection("jdbc:mysql://localhost:3306/manage","root","Anonimus1.");
	}

	public List<Comanda> refresh() throws SQLException{
		List<Comanda> lista = new ArrayList<Comanda>();
		Statement statement = (Statement) con.createStatement();
		ResultSet result = statement.executeQuery("select * from comanda");
		while(result.next()){
			Comanda temp = convertRowToComanda(result);
			lista.add(temp);
		}
		return lista;
		
	}
	public void insert(int ID_PRODUS,int ID_CLIENT,int CANTITATE,int TOTAL,String DATA) throws SQLException{
		String s1 = "INSERT into comanda (ID_PRODUS,ID_CLIENT,CANTITATE,TOTAL,DATA) values (?,?,?,?,?)";
		PreparedStatement st = (PreparedStatement) con.prepareStatement(s1);
		String s2="select Cost from produs where id_produs='"+ID_PRODUS+"'";
		PreparedStatement st2=(PreparedStatement) con.prepareStatement(s2);
		
		ResultSet rs=st2.executeQuery(s2);
		int c=1;
		if(rs.next()){
			  c=rs.getInt("Cost");
			}
		int total=c*CANTITATE;
		System.out.println(total);
		st.setInt(1, ID_PRODUS);
		st.setInt(2, ID_CLIENT);
		st.setInt(3, CANTITATE);
		st.setInt(4, total);
		st.setString(5, DATA);
		st.executeUpdate();
		st.close();
		st2.close();
		}
		public int total() throws SQLException{
			int sum = 0;
	        Statement st = (Statement) con.createStatement();
	        ResultSet res = st.executeQuery("SELECT SUM(total) FROM comanda");
	        while (res.next()) {
	        int c = res.getInt(1);
	        sum = sum + c;}
	        return sum;
		}
		public int setCost(int id_produs) throws SQLException{
			Statement st = (Statement) con.createStatement();
			int c=0;
	        ResultSet res = st.executeQuery("SELECT Cost FROM produs where id_produs='"+id_produs+"' ");
	        if (res.next() ){
	        	 c=res.getInt(1);
	            }
			return c;
		}
	public void delete(int ID_PRODUS) throws SQLException{
		PreparedStatement st = (PreparedStatement) con.prepareStatement("delete from comanda where ID_PRODUS='"+ID_PRODUS+"'");
		st.executeUpdate();
		st.close();
	}
	public void update(int ID,int ID_PRODUS,int ID_CLIENT,int CANTITATE,int TOTAL, String DATA) throws SQLException{
		PreparedStatement st = (PreparedStatement) con.prepareStatement("update comanda set ID_PRODUS='"+ID_PRODUS+"',ID_CLIENT='"+ID_CLIENT+"',CANTITATE='"+CANTITATE+"',TOTAL='"+TOTAL+"',DATA='"+DATA+"' where ID='"+ID+"'");
		st.executeUpdate();
		st.close();
	}
	
private Comanda convertRowToComanda(ResultSet myRs) throws SQLException {
		
		int ID = myRs.getInt("ID");
		int ID_PRODUS = myRs.getInt("ID_PRODUS");
		int ID_CLIENT = myRs.getInt("ID_CLIENT");
		int CANTITATE = myRs.getInt("CANTITATE");
		int TOTAL = myRs.getInt("TOTAL");
		String DATA = myRs.getString("DATA");
		
		
		Comanda temp = new Comanda(ID,ID_PRODUS,ID_CLIENT,CANTITATE,TOTAL,DATA);
		
		return temp;
	}

}
