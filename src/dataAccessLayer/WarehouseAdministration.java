package dataAccessLayer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

import businessLayer.Depozit;

public class WarehouseAdministration {

private Connection con;
	
	public WarehouseAdministration() throws SQLException{
		con = DriverManager.getConnection("jdbc:mysql://localhost:3306/manage","root","Anonimus1.");
	}
	
	public List<Depozit> refresh() throws SQLException{
		List<Depozit> lista = new ArrayList<Depozit>();
		Statement statement = (Statement) con.createStatement();
		ResultSet result = statement.executeQuery("select * from depozit");
		
		while(result.next()){
			Depozit temp = convertRowToDepozit(result);
			lista.add(temp);
		}
		return lista;
	}
	
	public void insert(int ID_PRODUS,int CANTITATE) throws SQLException{
		String s1 = "insert into depozit (ID_PRODUS,CANTITATE) values (?,?)";
		PreparedStatement st = (PreparedStatement) con.prepareStatement(s1);
		st.setInt(1, ID_PRODUS);
		st.setInt(2, CANTITATE);
		st.executeUpdate();
		st.close();
		
	}
	
	public void delete(int ID_PRODUS) throws SQLException{
		PreparedStatement st = (PreparedStatement) con.prepareStatement("delete from depozit where ID_PRODUS='"+ID_PRODUS+"'");
		st.executeUpdate();
		st.close();
	}
	public void update(int ID_PRODUS,int CANTITATE) throws SQLException{
		PreparedStatement st = (PreparedStatement) con.prepareStatement("update depozit set CANTITATE='"+CANTITATE+"' where ID_PRODUS='"+ID_PRODUS+"'");
		st.executeUpdate();
		st.close();
	}
	
	
	
	private Depozit convertRowToDepozit(ResultSet myRs) throws SQLException {
		
		int ID_PRODUS = myRs.getInt("ID_PRODUS");
		int CANTITATE = myRs.getInt("CANTITATE");
		
		Depozit temp = new Depozit(ID_PRODUS,CANTITATE);
		
		return temp;
	}
}
