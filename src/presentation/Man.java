package presentation;

import dataAccessLayer.ClientAdministration;
import dataAccessLayer.ProdusAdministration;
import dataAccessLayer.WarehouseAdministration;
import net.proteanit.sql.DbUtils;
import dataAccessLayer.ComandaAdministration;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTabbedPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import java.sql.*;
import javax.swing.table.DefaultTableModel;
import java.awt.Color;
import javax.swing.border.LineBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.io.PrintWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.JCheckBox;
import javax.swing.JScrollPane;
public class Man {

	private JFrame frmManagementClienti;
	private JTextField IDClientInsert;
	private JTextField IDClientUpdate;
	private JTextField IDClientDelete;
	private JTextField NumeClientDelete;
	private JTextField NumeClientInsert;
	private JTextField AdresaClientInsert;
	private JTextField TelClientInsert;
	private JTextField NumeClientUpdate;
	private JTextField AdresaClientUpdate;
	private JTextField TelClientUpdate;
	private JTextField IDProdusInsert;
	private JTextField NumeProdusInsert;
	private JTextField CostProdusInsert;
	private JTextField IDProdusDelete;
	private JTextField NumeProdusDelete;
	private JTextField IDProdusUpdate;
	private JTextField NumeProdusUpdate;
	private JTextField CostProdusUpdate;
	private JTextField NumeCComanda;
	private JTextField IDCComanda;
	private JTextField IDComanda;
	private JTextField CantitateComanda;
	private JTextField CantA;
	private JTextField CantU;
	private JTextField IDC;
	private JTextField DataC;
	private JTextField totalfield;
	private JTable table_1;
	private JTable tableCL;
	public int ok=-1;
	public int i=0;
	public Connection con=null;
	private JTextField CLIENTF;
	private JTable table;
	private JTextField DATAF;
	private JTextField textField;
	public Man() throws ClassNotFoundException, SQLException {
		initialize();
		  
		Class.forName("com.mysql.jdbc.Driver");
		con = DriverManager.getConnection("jdbc:mysql://localhost:3306/manage","root","Anonimus1.");
		
	}
	public void  UpdateTable() throws SQLException{
		
			String query="select * from client";
			PreparedStatement st=con.prepareStatement(query);
			ResultSet rs=st.executeQuery();
			tableCL.setModel(DbUtils.resultSetToTableModel(rs));
		st.close();
		rs.close();
	}
	public void  UpdateTableP() throws SQLException{
		
		String query="select * from produs";
		PreparedStatement st=con.prepareStatement(query);
		ResultSet rs=st.executeQuery();
		table_1.setModel(DbUtils.resultSetToTableModel(rs));
	st.close();
	rs.close();
}
	public void UpdateTableC() throws SQLException{
		String query="select * from comanda";
		PreparedStatement st=con.prepareStatement(query);
		ResultSet rs=st.executeQuery();
		table.setModel(DbUtils.resultSetToTableModel(rs));
	st.close();
	rs.close();
	}
	@SuppressWarnings("serial")
	private void initialize() {
		frmManagementClienti = new JFrame();
		frmManagementClienti.setTitle("Management ");
		frmManagementClienti.setBounds(100, 100, 693, 540);
		frmManagementClienti.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmManagementClienti.getContentPane().setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 11, 657, 428);
		frmManagementClienti.getContentPane().add(tabbedPane);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Management Clienti", null, panel, null);
		panel.setLayout(null);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new LineBorder(new Color(0, 0, 0), 4));
		panel_3.setBounds(10, 11, 287, 133);
		panel.add(panel_3);
		panel_3.setLayout(null);
		
		JLabel lblAdaugare = new JLabel("Adaugare");
		lblAdaugare.setBounds(10, 11, 71, 14);
		panel_3.add(lblAdaugare);
		
		IDClientInsert = new JTextField();
		IDClientInsert.setBounds(242, 8, 35, 20);
		panel_3.add(IDClientInsert);
		IDClientInsert.setColumns(10);
		
		JLabel lblId = new JLabel("ID:");
		lblId.setBounds(214, 11, 18, 14);
		panel_3.add(lblId);
		
		JButton btnAdaugareClient = new JButton("Adaugare Client");
		btnAdaugareClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
				 
				try {
					try {
						RaportClienti rapp = new RaportClienti();
						rapp.UpdateTableRapC();
					} catch (ClassNotFoundException | SQLException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
					  
					String query="select * from client";
					PreparedStatement st=con.prepareStatement(query);
					ResultSet rs=st.executeQuery();
					tableCL.setModel(DbUtils.resultSetToTableModel(rs));
					st.close();
					rs.close();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				PreparedStatement st;
				
				
				String nume=NumeClientInsert.getText();
				String tel=TelClientInsert.getText();
				String adresa=AdresaClientInsert.getText();
				NumeClientUpdate.setText(nume);
				TelClientUpdate.setText(tel);
				AdresaClientUpdate.setText(adresa);
				NumeClientDelete.setText(nume);
				try {
					String query="select ID from client where Nume='"+nume+"'";
					st = con.prepareStatement(query);
					ResultSet rs=st.executeQuery();
					if(rs.next()){
						IDClientUpdate.setText(rs.getString("ID"));
						IDClientDelete.setText(rs.getString("ID"));
					}
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
				ClientAdministration client=new ClientAdministration();
					client.insertClient(nume,tel,adresa);
					NumeClientInsert.setText("");
					AdresaClientInsert.setText("");
					IDClientInsert.setText("");
					TelClientInsert.setText("");
				} catch (ClassNotFoundException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					UpdateTable();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
	});
	btnAdaugareClient.setBounds(10, 99, 267, 23);
		panel_3.add(btnAdaugareClient);
		
		JLabel lblNewLabel_1 = new JLabel("Nume:");
		lblNewLabel_1.setBounds(20, 36, 46, 14);
		panel_3.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Adresa:");
		lblNewLabel_2.setBounds(20, 58, 46, 14);
		panel_3.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Tel:");
		lblNewLabel_3.setBounds(20, 83, 46, 14);
		panel_3.add(lblNewLabel_3);
		
		NumeClientInsert = new JTextField();
		NumeClientInsert.setBounds(76, 33, 86, 17);
		panel_3.add(NumeClientInsert);
		NumeClientInsert.setColumns(10);
		
		AdresaClientInsert = new JTextField();
		AdresaClientInsert.setBounds(76, 56, 86, 17);
		panel_3.add(AdresaClientInsert);
		AdresaClientInsert.setColumns(10);
		
		TelClientInsert = new JTextField();
		TelClientInsert.setBounds(76, 81, 86, 17);
		panel_3.add(TelClientInsert);
		TelClientInsert.setColumns(10);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new LineBorder(new Color(0, 0, 0), 4));
		panel_4.setBounds(10, 296, 287, 93);
		panel.add(panel_4);
		panel_4.setLayout(null);
		
		JLabel lblStergere = new JLabel("Stergere");
		lblStergere.setBounds(10, 11, 71, 14);
		panel_4.add(lblStergere);
		
		JLabel lblId_2 = new JLabel("ID:");
		lblId_2.setBounds(120, 11, 28, 14);
		panel_4.add(lblId_2);
		
		IDClientDelete = new JTextField();
		IDClientDelete.setColumns(10);
		IDClientDelete.setBounds(157, 8, 35, 20);
		panel_4.add(IDClientDelete);
		
		JLabel lblNewLabel = new JLabel("Nume:");
		lblNewLabel.setBounds(104, 36, 44, 14);
		panel_4.add(lblNewLabel);
		
		NumeClientDelete = new JTextField();
		NumeClientDelete.setBounds(157, 33, 86, 20);
		panel_4.add(NumeClientDelete);
		NumeClientDelete.setColumns(10);
		
		JButton btnNewButton = new JButton("Stergere Client");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					try {
						RaportClienti rapp = new RaportClienti();
						rapp.UpdateTableRapC();
					} catch (ClassNotFoundException | SQLException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
					String query="select * from client";
					PreparedStatement st=con.prepareStatement(query);
					ResultSet rs=st.executeQuery();
					tableCL.setModel(DbUtils.resultSetToTableModel(rs));
					st.close();
					rs.close();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				String nume=NumeClientDelete.getText();
				try {
					UpdateTable();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					ClientAdministration client=new ClientAdministration();
					if(NumeClientDelete.getText().equals("")){
						int ID=Integer.parseInt(IDClientDelete.getText());
					client.deleteClientID(ID);}
					if(IDClientDelete.getText().equals("")){
						client.deleteClientNume(nume);
						
						
					}
					
				} catch (ClassNotFoundException | SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				IDClientDelete.setText("");
				NumeClientDelete.setText("");
				try {
					UpdateTable();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		
		});
		btnNewButton.setBounds(10, 61, 267, 23);
		panel_4.add(btnNewButton);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBorder(new LineBorder(new Color(0, 0, 0), 4));
		panel_5.setBounds(10, 155, 287, 130);
		panel.add(panel_5);
		panel_5.setLayout(null);
		
		JLabel lblUpdate = new JLabel("Update");
		lblUpdate.setBounds(10, 11, 71, 14);
		panel_5.add(lblUpdate);
		
		JLabel lblId_1 = new JLabel("ID:");
		lblId_1.setBounds(214, 11, 18, 14);
		panel_5.add(lblId_1);
		
		IDClientUpdate = new JTextField();
		IDClientUpdate.setColumns(10);
		IDClientUpdate.setBounds(242, 8, 35, 20);
		panel_5.add(IDClientUpdate);
		
		JButton btnUpdateClient = new JButton("Update Client");
		btnUpdateClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					try {
						RaportClienti rapp = new RaportClienti();
						rapp.UpdateTableRapC();
					} catch (ClassNotFoundException | SQLException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
					String query="select * from client";
					PreparedStatement st=con.prepareStatement(query);
					ResultSet rs=st.executeQuery();
					tableCL.setModel(DbUtils.resultSetToTableModel(rs));
					st.close();
					rs.close();
					
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				String nume=NumeClientUpdate.getText();
				String adresa=AdresaClientUpdate.getText();
				String tel=TelClientUpdate.getText();
				int id=Integer.parseInt(IDClientUpdate.getText());
				System.out.println(id);
				try {
					
					ClientAdministration client=new ClientAdministration();
					client.updateClient(id, nume, tel, adresa);
					NumeClientUpdate.setText("");
					AdresaClientUpdate.setText("");
					IDClientUpdate.setText("");
					TelClientUpdate.setText("");
				} catch (ClassNotFoundException | SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					UpdateTable();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnUpdateClient.setBounds(10, 96, 267, 23);
		panel_5.add(btnUpdateClient);
		
		JLabel label = new JLabel("Nume:");
		label.setBounds(20, 28, 46, 14);
		panel_5.add(label);
		
		NumeClientUpdate = new JTextField();
		NumeClientUpdate.setColumns(10);
		NumeClientUpdate.setBounds(76, 25, 86, 17);
		panel_5.add(NumeClientUpdate);
		
		AdresaClientUpdate = new JTextField();
		AdresaClientUpdate.setColumns(10);
		AdresaClientUpdate.setBounds(76, 48, 86, 17);
		panel_5.add(AdresaClientUpdate);
		
		JLabel label_1 = new JLabel("Adresa:");
		label_1.setBounds(20, 50, 46, 14);
		panel_5.add(label_1);
		
		JLabel label_2 = new JLabel("Tel:");
		label_2.setBounds(20, 75, 46, 14);
		panel_5.add(label_2);
		
		TelClientUpdate = new JTextField();
		TelClientUpdate.setColumns(10);
		TelClientUpdate.setBounds(76, 73, 86, 17);
		panel_5.add(TelClientUpdate);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(318, 11, 324, 378);
		panel.add(scrollPane_1);
		
		tableCL = new JTable();
		scrollPane_1.setViewportView(tableCL);
		tableCL.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"ID", "Nume", "Telefon", "Adresa"
			}
		) {
			Class[] columnTypes = new Class[] {
				String.class, Object.class, Object.class, Object.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
			boolean[] columnEditables = new boolean[] {
				false, true, true, true
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		tableCL.getColumnModel().getColumn(0).setResizable(false);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Management Produse", null, panel_1, null);
		panel_1.setLayout(null);
		
		JPanel panel_6 = new JPanel();
		panel_6.setLayout(null);
		panel_6.setBounds(0, 0, 652, 400);
		panel_1.add(panel_6);
		
		JPanel panel_7 = new JPanel();
		panel_7.setLayout(null);
		panel_7.setBorder(new LineBorder(new Color(0, 0, 0), 4));
		panel_7.setBounds(10, 11, 287, 133);
		panel_6.add(panel_7);
		
		JLabel label_3 = new JLabel("Adaugare");
		label_3.setBounds(10, 11, 71, 14);
		panel_7.add(label_3);
		
		IDProdusInsert = new JTextField();
		IDProdusInsert.setColumns(10);
		IDProdusInsert.setBounds(242, 8, 35, 20);
		panel_7.add(IDProdusInsert);
		
		JLabel label_4 = new JLabel("ID:");
		label_4.setBounds(214, 11, 18, 14);
		panel_7.add(label_4);
		
		JButton btnAdaugareProdus = new JButton("Adaugare Produs");
		btnAdaugareProdus.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				IDProdusUpdate.setText(IDProdusInsert.getText());
				NumeProdusUpdate.setText(NumeProdusInsert.getText());
				CostProdusUpdate.setText(CostProdusInsert.getText());
				CantU.setText(CantA.getText());
				NumeProdusDelete.setText(NumeProdusInsert.getText());
				IDProdusDelete.setText(IDProdusInsert.getText());
				try {
					try {
						RaportProduse rapp = new RaportProduse();
						rapp.populateTable();;
					} catch (ClassNotFoundException | SQLException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
					String query="select * from produs";
					PreparedStatement st=con.prepareStatement(query);
					ResultSet rs=st.executeQuery();
					table_1.setModel(DbUtils.resultSetToTableModel(rs));
					st.close();
					rs.close();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				int id=Integer.parseInt(IDProdusInsert.getText());
				String nume=NumeProdusInsert.getText();
				int cost=Integer.parseInt(CostProdusInsert.getText());
				
				try {
					
					ProdusAdministration prod=new ProdusAdministration();
					prod.insertProdus(id,nume, cost);
				
					
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					UpdateTableP();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				WarehouseAdministration w;
				try {
					w = new WarehouseAdministration();
					w.insert(id, Integer.parseInt(CantA.getText()));
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				IDProdusInsert.setText("");
				NumeProdusInsert.setText("");
				CantA.setText("");
				CostProdusInsert.setText("");
			}
		});
		btnAdaugareProdus.setBounds(10, 99, 267, 23);
		panel_7.add(btnAdaugareProdus);
		
		JLabel label_5 = new JLabel("Nume:");
		label_5.setBounds(20, 33, 46, 14);
		panel_7.add(label_5);
		
		JLabel lblCost = new JLabel("Cost:");
		lblCost.setBounds(20, 61, 46, 14);
		panel_7.add(lblCost);
		
		NumeProdusInsert = new JTextField();
		NumeProdusInsert.setColumns(10);
		NumeProdusInsert.setBounds(76, 36, 86, 17);
		panel_7.add(NumeProdusInsert);
		
		CostProdusInsert = new JTextField();
		CostProdusInsert.setColumns(10);
		CostProdusInsert.setBounds(76, 59, 86, 17);
		panel_7.add(CostProdusInsert);
		
		CantA = new JTextField();
		CantA.setColumns(10);
		CantA.setBounds(76, 81, 86, 17);
		panel_7.add(CantA);
		
		JLabel lblCant = new JLabel("Cant:");
		lblCant.setBounds(20, 86, 46, 14);
		panel_7.add(lblCant);
		
		JPanel panel_8 = new JPanel();
		panel_8.setLayout(null);
		panel_8.setBorder(new LineBorder(new Color(0, 0, 0), 4));
		panel_8.setBounds(10, 296, 287, 93);
		panel_6.add(panel_8);
		
		JLabel label_8 = new JLabel("Stergere");
		label_8.setBounds(10, 11, 71, 14);
		panel_8.add(label_8);
		
		JLabel label_9 = new JLabel("ID:");
		label_9.setBounds(123, 11, 25, 14);
		panel_8.add(label_9);
		
		IDProdusDelete = new JTextField();
		IDProdusDelete.setColumns(10);
		IDProdusDelete.setBounds(157, 8, 35, 20);
		panel_8.add(IDProdusDelete);
		
		JLabel label_10 = new JLabel("Nume:");
		label_10.setBounds(107, 36, 41, 14);
		panel_8.add(label_10);
		
		NumeProdusDelete = new JTextField();
		NumeProdusDelete.setColumns(10);
		NumeProdusDelete.setBounds(157, 33, 86, 20);
		panel_8.add(NumeProdusDelete);
		
		JButton btnStergereProdus = new JButton("Stergere Produs");
		btnStergereProdus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					try {
						RaportProduse rapp = new RaportProduse();
						rapp.populateTable();
					} catch (ClassNotFoundException | SQLException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
					String query="select * from produs";
					PreparedStatement st=con.prepareStatement(query);
					ResultSet rs=st.executeQuery();
					table_1.setModel(DbUtils.resultSetToTableModel(rs));
					st.close();
					rs.close();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				String nume=NumeProdusDelete.getText();
				try {
					ProdusAdministration prod=new ProdusAdministration();
					
					if(NumeProdusDelete.getText().equals("")){
						int id=Integer.parseInt(IDProdusDelete.getText());
						prod.deleteProdusID(id);
					}
					if(IDProdusDelete.getText().equals("")){
						prod.deleteProdusNume(nume);
					}
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				NumeProdusDelete.setText("");
				IDProdusDelete.setText("");
				try {
					UpdateTableP();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnStergereProdus.setBounds(10, 61, 267, 23);
		panel_8.add(btnStergereProdus);
		
		JPanel panel_9 = new JPanel();
		panel_9.setLayout(null);
		panel_9.setBorder(new LineBorder(new Color(0, 0, 0), 4));
		panel_9.setBounds(10, 155, 287, 130);
		panel_6.add(panel_9);
		
		JLabel label_11 = new JLabel("Update");
		label_11.setBounds(10, 11, 71, 14);
		panel_9.add(label_11);
		
		JLabel label_12 = new JLabel("ID:");
		label_12.setBounds(214, 11, 18, 14);
		panel_9.add(label_12);
		
		IDProdusUpdate = new JTextField();
		IDProdusUpdate.setColumns(10);
		IDProdusUpdate.setBounds(242, 8, 35, 20);
		panel_9.add(IDProdusUpdate);
		
		JButton btnUpdateProdus = new JButton("Update Produs");
		btnUpdateProdus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					try {
						RaportProduse rapp = new RaportProduse();
						rapp.populateTable();;
					} catch (ClassNotFoundException | SQLException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
					String query="select * from produs";
					PreparedStatement st=con.prepareStatement(query);
					ResultSet rs=st.executeQuery();
					table_1.setModel(DbUtils.resultSetToTableModel(rs));
					st.close();
					rs.close();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				int id=Integer.parseInt(IDProdusUpdate.getText());
				String nume=NumeProdusUpdate.getText();
				int cost=Integer.parseInt(CostProdusUpdate.getText());
				try {
					ProdusAdministration prod=new ProdusAdministration();
					prod.updateProdus(id, nume, cost);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					UpdateTableP();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					UpdateTableC();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnUpdateProdus.setBounds(10, 96, 267, 23);
		panel_9.add(btnUpdateProdus);
		
		JLabel label_13 = new JLabel("Nume:");
		label_13.setBounds(20, 28, 46, 14);
		panel_9.add(label_13);
		
		NumeProdusUpdate = new JTextField();
		NumeProdusUpdate.setColumns(10);
		NumeProdusUpdate.setBounds(76, 25, 86, 17);
		panel_9.add(NumeProdusUpdate);
		
		JLabel lblCost_1 = new JLabel("Cost:");
		lblCost_1.setBounds(20, 53, 46, 14);
		panel_9.add(lblCost_1);
		
		CostProdusUpdate = new JTextField();
		CostProdusUpdate.setColumns(10);
		CostProdusUpdate.setBounds(76, 53, 86, 17);
		panel_9.add(CostProdusUpdate);
		
		CantU = new JTextField();
		CantU.setColumns(10);
		CantU.setBounds(76, 78, 86, 17);
		panel_9.add(CantU);
		
		JLabel label_14 = new JLabel("Cant:");
		label_14.setBounds(20, 78, 46, 14);
		panel_9.add(label_14);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(328, 11, 314, 378);
		panel_6.add(scrollPane);
		
		table_1 = new JTable();
		scrollPane.setViewportView(table_1);
		table_1.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
			}
		));
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Comenzi", null, panel_2, null);
		panel_2.setLayout(null);
		
		JPanel panel_10 = new JPanel();
		panel_10.setBorder(new LineBorder(new Color(0, 0, 0), 3, true));
		panel_10.setBounds(10, 11, 269, 378);
		panel_2.add(panel_10);
		panel_10.setLayout(null);
		
		
		JCheckBox ckBon = new JCheckBox("Bon fiscal");
		ckBon.setBounds(32, 303, 94, 23);
		panel_10.add(ckBon); 
		
		JTabbedPane tabbedPane_1 = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane_1.setBounds(10, 45, 249, 217);
		panel_10.add(tabbedPane_1);
		
		JPanel panel_11 = new JPanel();
		tabbedPane_1.addTab("Client", null, panel_11, null);
		panel_11.setLayout(null);
		
		JCheckBox ckCurier = new JCheckBox("Curier");
		ckCurier.setBounds(97, 79, 97, 23);
		panel_11.add(ckCurier);
		
		JCheckBox ckRidicarePersonala = new JCheckBox("Ridicare Personala");
		ckRidicarePersonala.setBounds(97, 105, 141, 23);
		panel_11.add(ckRidicarePersonala);
		
		JCheckBox ckPostaRomana = new JCheckBox("Posta Romana");
		ckPostaRomana.setBounds(97, 134, 97, 23);
		panel_11.add(ckPostaRomana);
		
		JButton btnPlasareComanda = new JButton("Plasare Comanda");
		btnPlasareComanda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (ckBon.isSelected()){
					
				  
					try {
						int total=0;
						ComandaAdministration com=new ComandaAdministration();
						FileWriter file = new FileWriter ("C:/Users/kovac_000/Desktop/Laborator3_tp/bon.txt");
						PrintWriter printWriter = new PrintWriter (file);
						String queryC="Select ID_PRODUS from comanda";
						printWriter.println ("-FACTURA-");
						printWriter.println ("Clientul:"+" "+CLIENTF.getText());
						printWriter.println ("-"+Integer.parseInt(IDC.getText())+"-");
						printWriter.println ("Produse achizitionate:");
						PreparedStatement stC=con.prepareStatement(queryC);
						ResultSet rs=stC.executeQuery(queryC);
						while(rs.next()){
							String queryP="Select Nume_produs from produs where id_produs='"+rs.getString("ID_PRODUS")+"'";
							PreparedStatement stP=con.prepareStatement(queryP);
							ResultSet rsP=stP.executeQuery(queryP);
							String queryCCant="Select CANTITATE from comanda where ID_PRODUS='"+rs.getString("ID_PRODUS")+"'";
							PreparedStatement stCC=con.prepareStatement(queryCCant);
							ResultSet rsCC=stCC.executeQuery(queryCCant);
							String queryCost="Select TOTAL from comanda where ID_PRODUS='"+rs.getString("ID_PRODUS")+"'";
							PreparedStatement stCost=con.prepareStatement(queryCost);
							ResultSet rsCost=stCost.executeQuery(queryCost);
							while(rsP.next()){
								while(rsCC.next()){
									while(rsCost.next()){
									printWriter.println (Integer.parseInt(rsCC.getString("CANTITATE"))+" xbuc de "+rsP.getString("Nume_produs")+" la pretul de: "+Integer.parseInt(rsCost.getString("TOTAL")));
								}
							}
						}
					}
						if(ckRidicarePersonala.isSelected()){
						printWriter.println ("Metoda de livrare: Ridicare Personala(nu se percep taxe suplimentare)");}
						if(ckCurier.isSelected()){
							printWriter.println ("Metoda de livrare: Servicii de curierat(se percep taxe suplimentare in valoare de 25 lei)");}
						if(ckPostaRomana.isSelected()){
							printWriter.println ("Metoda de livrare: Posta Romana(se percep taxe suplimentare in valoare de 10 lei");}
						printWriter.println ("Discount:"+" "+Integer.parseInt(textField.getText()));
						if(ckRidicarePersonala.isSelected()){
							total=com.total()-com.total()*Integer.parseInt(textField.getText())/100;
						printWriter.println ("Total fara TVA:"+" "+total);
						printWriter.println ("Total cu TVA:"+" "+total+total*25/100);}
						if(ckCurier.isSelected()){
							total=com.total()-com.total()*Integer.parseInt(textField.getText())/100+25;
							printWriter.println ("Total fara TVA:"+" "+total);
							printWriter.println ("Total cu TVA:"+" "+total+total*25/100);}
						if(ckPostaRomana.isSelected()){
							total=com.total()-com.total()*Integer.parseInt(textField.getText())/100+10;
							printWriter.println ("Total fara TVA:"+" "+total);
							printWriter.println ("Total cu TVA:"+" "+total+total*25/100);}
						printWriter.println ("Tranzactie incheiata de:");
						printWriter.println ("Data:"+" "+DATAF.getText());
					    printWriter.close ();  
					   
					   
					    
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				       
				}
					
				
				
			}
		});
		btnPlasareComanda.setBounds(10, 333, 249, 34);
		panel_10.add(btnPlasareComanda);
		
		JButton btnNewButton_1 = new JButton("Raport Produse");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RaportProduse rapP;
				try {
					rapP = new RaportProduse();
					rapP.setVisible(true);
					rapP.populateTable();
				} catch (ClassNotFoundException | SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
			}
		});
		btnNewButton_1.setBounds(143, 11, 116, 23);
		panel_10.add(btnNewButton_1);
		
		JButton btnRaportClienti = new JButton("Raport Clienti");
		btnRaportClienti.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RaportClienti rapC;
				try {
					rapC = new RaportClienti();
							rapC.setVisiblee(true);
							rapC.populateTable();
				} catch (ClassNotFoundException | SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
			}
		});
		btnRaportClienti.setBounds(10, 11, 116, 23);
		panel_10.add(btnRaportClienti);
		
		
		
		JPanel panel_12 = new JPanel();
		tabbedPane_1.addTab("Produs", null, panel_12, null);
		panel_12.setLayout(null);
		
		JLabel lblId_3 = new JLabel("ID:*");
		lblId_3.setBounds(10, 11, 46, 14);
		panel_12.add(lblId_3);
		
		IDComanda = new JTextField();
		IDComanda.setColumns(10);
		IDComanda.setBounds(97, 8, 86, 20);
		panel_12.add(IDComanda);
		
		CantitateComanda = new JTextField();
		CantitateComanda.setBounds(97, 34, 86, 20);
		panel_12.add(CantitateComanda);
		CantitateComanda.setColumns(10);
		
		JLabel lblCantitate = new JLabel("Cantitate:*");
		lblCantitate.setBounds(10, 37, 77, 14);
		panel_12.add(lblCantitate);
		
		
		
		JLabel lblDiscount = new JLabel("Discount:");
		lblDiscount.setBounds(10, 62, 77, 14);
		panel_12.add(lblDiscount);
		
		JButton btnAdaugaInCos = new JButton("Adauga in cos");
		btnAdaugaInCos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				try {
					Depozit depp=new Depozit();
					depp.PopulateTable();
					int idp=Integer.parseInt(IDComanda.getText());
					String queryD="SELECT CANTITATE from depozit WHERE ID_PRODUS ='"+idp+"'";
					int cant=Integer.parseInt(CantitateComanda.getText());
					PreparedStatement stD=con.prepareStatement(queryD);
					ResultSet rsD=stD.executeQuery(queryD);
					String data=DataC.getText();
					
					
					int idc=Integer.parseInt(IDCComanda.getText());
		String query = "SELECT ID from client WHERE ID ='"+Integer.parseInt(IDCComanda.getText())+"'";
		String queryCheck = "SELECT id_produs from produs WHERE id_produs ='"+Integer.parseInt(IDComanda.getText())+"'";
									
					
					
					while(rsD.next()){
						
						if(cant<=rsD.getInt("CANTITATE")){
							WarehouseAdministration wr=new WarehouseAdministration();
							wr.update(idp, rsD.getInt("CANTITATE")-cant);
							try {
								
								PreparedStatement st0 = con.prepareStatement(queryCheck);
								ResultSet rs = st0.executeQuery(queryCheck);
								PreparedStatement st3 = con.prepareStatement(query);
								ResultSet rs3 = st3.executeQuery(query);
								if(rs3.absolute(1)&&rs.absolute(1)){
									try {
										
										CLIENTF.setText(NumeCComanda.getText());
										DATAF.setText(DataC.getText());
										String query_1="select * from comanda";
										PreparedStatement st_1=con.prepareStatement(query_1);
										ResultSet rs_1=st_1.executeQuery();
										
										
										table.setModel(DbUtils.resultSetToTableModel(rs_1));
										
									} catch (SQLException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
									try {
										ComandaAdministration comanda=new ComandaAdministration();
									int	total=comanda.setCost(idp); 
										totalfield.setText(Integer.toString(comanda.total()));
										comanda.insert( idp, idc, cant, total, data);
									} catch (SQLException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
									
								}
								else if(!rs3.absolute(1)&&!rs.absolute(1))
									JOptionPane.showMessageDialog(null,"Client,Produs:Introduceti un id valid!");
								else if(!rs.absolute(1))
								 JOptionPane.showMessageDialog(null,"Produs:Introduceti un id valid!");
								else if(!rs3.absolute(1))
									JOptionPane.showMessageDialog(null,"Client:Introduceti un id valid!");
								
							} catch (SQLException e2) {
								// TODO Auto-generated catch block
								e2.printStackTrace();
							}

						}
						else
							JOptionPane.showMessageDialog(null, "Nu sunt destule bucati pe stoc!");
						
							
					}
				} catch (SQLException e3) {
					// TODO Auto-generated catch block
					e3.printStackTrace();
				} catch (ClassNotFoundException e3) {
					// TODO Auto-generated catch block
					e3.printStackTrace();
				}
				try {
					UpdateTableC();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnAdaugaInCos.setBounds(10, 90, 224, 23);
		panel_12.add(btnAdaugaInCos);
		
		JButton btnStergereProdus_1 = new JButton("Stergere Produs");
		btnStergereProdus_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
try {
	ComandaAdministration com=new ComandaAdministration();
	int id_prd=Integer.parseInt(IDComanda.getText());
	com.delete(id_prd);
} catch (SQLException e1) {
	// TODO Auto-generated catch block
	e1.printStackTrace();
}
try {
	UpdateTableC();
} catch (SQLException e1) {
	// TODO Auto-generated catch block
	e1.printStackTrace();
}
			}
		});
		btnStergereProdus_1.setBounds(10, 124, 224, 23);
		panel_12.add(btnStergereProdus_1);
		
		textField = new JTextField();
		textField.setBounds(97, 59, 86, 17);
		panel_12.add(textField);
		textField.setColumns(10);
		
		
		
		NumeCComanda = new JTextField();
		NumeCComanda.setBounds(97, 11, 86, 20);
		panel_11.add(NumeCComanda);
		NumeCComanda.setColumns(10);
		
		IDCComanda = new JTextField();
		IDCComanda.setBounds(97, 42, 86, 20);
		panel_11.add(IDCComanda);
		IDCComanda.setColumns(10);
		
		
		
		JLabel lblNume = new JLabel("Nume:*");
		lblNume.setBounds(10, 14, 46, 14);
		panel_11.add(lblNume);
		
		JLabel lblNewLabel_4 = new JLabel("ID:*");
		lblNewLabel_4.setBounds(10, 45, 46, 14);
		panel_11.add(lblNewLabel_4);
		
		JTextPane txtpnMetoda = new JTextPane();
		txtpnMetoda.setBackground(Color.LIGHT_GRAY);
		txtpnMetoda.setText("Mod de transport:*");
		txtpnMetoda.setBounds(10, 79, 81, 34);
		panel_11.add(txtpnMetoda);
		
		
		
		JButton btnVerificareTotal = new JButton("Verificare Total");
		btnVerificareTotal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					ComandaAdministration com=new ComandaAdministration();
					int total=0;
					if(!ckCurier.isSelected()&&!ckRidicarePersonala.isSelected()&&!ckPostaRomana.isSelected()){
						JOptionPane.showMessageDialog(null, "Bifati o modalitate de transport");
					}
					else if(ckRidicarePersonala.isSelected()){
						ok=0;
						int dsc;
						if(textField.getText().equals("")){
							dsc=0;
						}
						else dsc=Integer.parseInt(textField.getText());
							
						total=com.total()-com.total()*dsc/100;
					totalfield.setText(Integer.toString(total)+" "+"lei");}
					else if(ckCurier.isSelected()){
						ok=1;
						int dsc;
						if(textField.getText().equals("")){
							dsc=0;
						}
						else dsc=Integer.parseInt(textField.getText());
						total=com.total()-com.total()*dsc/100+25;
						totalfield.setText(Integer.toString(total)+" "+"lei");}
					else if (ckPostaRomana.isSelected()){
						ok=2;
						int dsc;
						if(textField.getText().equals("")){
							dsc=0;
						}
						else dsc=Integer.parseInt(textField.getText());
						total=com.total()-com.total()*dsc/100+10;
						totalfield.setText(Integer.toString(total)+" "+"lei");
				}}
				 catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnVerificareTotal.setBounds(20, 273, 122, 23);
		panel_10.add(btnVerificareTotal);
		
		DataC = new JTextField();
		DataC.setColumns(10);
		DataC.setBounds(194, 304, 52, 20);
		panel_10.add(DataC);
		
		JLabel lblDatal = new JLabel("Data:*");
		lblDatal.setBounds(143, 307, 46, 14);
		panel_10.add(lblDatal);
		
		totalfield = new JTextField();
		totalfield.setBounds(150, 274, 76, 20);
		panel_10.add(totalfield);
		totalfield.setColumns(10);
		
		CLIENTF = new JTextField();
		CLIENTF.setBounds(517, 11, 125, 20);
		panel_2.add(CLIENTF);
		CLIENTF.setColumns(10);
		
		JLabel lblClient = new JLabel("Client:");
		lblClient.setBounds(459, 13, 48, 17);
		panel_2.add(lblClient);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(289, 63, 353, 326);
		panel_2.add(scrollPane_2);
		
		table = new JTable();
		scrollPane_2.setViewportView(table);
		
		DATAF = new JTextField();
		DATAF.setBounds(368, 11, 76, 20);
		panel_2.add(DATAF);
		DATAF.setColumns(10);
		
		JLabel lblNewLabel_6 = new JLabel("Data:");
		lblNewLabel_6.setBounds(305, 14, 46, 14);
		panel_2.add(lblNewLabel_6);
		
		JButton dbtn = new JButton("Depozit");
		dbtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				try {
					Depozit dep = new Depozit();
					dep.PopulateTable();
					dep.setVisible(true);
				} catch (ClassNotFoundException | SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					Depozit dep = new Depozit();
					dep.PopulateTable();
				} catch (ClassNotFoundException | SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		dbtn.setBounds(527, 32, 116, 20);
		panel_2.add(dbtn);
		
		JLabel lblNewLabel_5 = new JLabel("ID C:*");
		lblNewLabel_5.setBounds(305, 38, 34, 14);
		panel_2.add(lblNewLabel_5);
		
		IDC = new JTextField();
		IDC.setBounds(368, 33, 76, 19);
		panel_2.add(IDC);
		IDC.setColumns(10);
		
		JButton btnNewButton_2 = new JButton("X");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 String del="Delete from comanda";
				    PreparedStatement st;
					try {
						
						st = con.prepareStatement(del);
						st.execute();
						   st.close();
					
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					try {
						UpdateTableC();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			}
		});
		btnNewButton_2.setForeground(Color.RED);
		btnNewButton_2.setBackground(Color.RED);
		btnNewButton_2.setBounds(459, 31, 48, 23);
		panel_2.add(btnNewButton_2);
		
		JButton btnLogOut = new JButton("Log out");
		btnLogOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			YesNo sign=new YesNo();
			sign.setVisible(true);
			frmManagementClienti.setVisible(false);
			
			}
		});
		btnLogOut.setBounds(294, 467, 89, 23);
		frmManagementClienti.getContentPane().add(btnLogOut);
		
		JButton btnReset = new JButton("RESET");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String delCom="Delete from comanda";
				String delCl="Delete from client";
				String delPr ="Delete from produs";
				String delDep="Delete from depozit";
				String delUs="Delete from useri";
			    PreparedStatement stCom;
			    PreparedStatement stCl;
			    PreparedStatement stPr;
			    PreparedStatement stDep;
			    PreparedStatement stUs;
				try {
					
					stCom = con.prepareStatement(delCom);
					stCl = con.prepareStatement(delCl);
					stPr = con.prepareStatement(delPr);
					stDep = con.prepareStatement(delDep);
					stUs = con.prepareStatement(delUs);
					stCom.execute();
					stCl.execute();
					stPr.execute();
					stUs.execute();
					stDep.execute();
					   stCom.close();
					   stCl.close();
					   stPr.close();
					   stUs.close();
					   stDep.close();
				
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					UpdateTableC();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnReset.setBounds(10, 467, 89, 23);
		frmManagementClienti.getContentPane().add(btnReset);
	}

	public void setVisible(boolean b) {
		frmManagementClienti.setVisible(true);
		
	}
}
