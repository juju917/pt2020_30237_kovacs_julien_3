package presentation;


import java.sql.*;
import javax.swing.JFrame;
import javax.swing.JTable;
import net.proteanit.sql.DbUtils;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
public class RaportProduse {

	private JFrame frmRaportProduse;
	private JTable table;
	private JScrollPane scrollPane;

Connection con=null;

	public RaportProduse() throws ClassNotFoundException, SQLException {
		initialize();
		Class.forName("com.mysql.jdbc.Driver");
		con = DriverManager.getConnection("jdbc:mysql://localhost:3306/manage","root","Anonimus1.");
	}

	public void populateTable(){
		try {
			String query="select * from produs";
			PreparedStatement st=con.prepareStatement(query);
			ResultSet rs=st.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(rs));
			st.close();
			rs.close();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	private void initialize() {
		frmRaportProduse = new JFrame();
		frmRaportProduse.setTitle("Raport Produse");
		frmRaportProduse.setBounds(100, 100, 450, 453);
		frmRaportProduse.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmRaportProduse.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("Close");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frmRaportProduse.setVisible(false);
			}
		});
		btnNewButton.setBounds(119, 387, 200, 16);
		frmRaportProduse.getContentPane().add(btnNewButton);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 414, 363);
		frmRaportProduse.getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
	}

	public void setVisible(boolean b) {
		frmRaportProduse.setVisible(true);
		
	}

}
