package presentation;



import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Pop {

	private JFrame frame;
	private JTextField textField;
	public Pop() {
		initialize();
	}
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 185, 149);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(36, 36, 86, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Given Code");
		lblNewLabel.setBounds(38, 11, 137, 29);
		frame.getContentPane().add(lblNewLabel);
		
		JButton btnProceed = new JButton("Proceed");
		btnProceed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(textField.getText().equals("MNEBZKEY")){
				SignUp SU=new SignUp();
				SU.setVisible(true);
				frame.setVisible(false);
				}
				else
					JOptionPane.showMessageDialog(null,"Wrong Code");
					textField.setText("");
			}
			
		});
		btnProceed.setBounds(36, 65, 86, 41);
		frame.getContentPane().add(btnProceed);
	}

	public void setVisible(boolean b) {
		frame.setVisible(true);
		
	}

}
