package presentation;



import javax.swing.JFrame;
import javax.swing.JTable;
import net.proteanit.sql.DbUtils;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import java.sql.*;
public class RaportClienti {

	private JFrame frmRaportClineti;
	private JTable table;
	private JScrollPane scrollPane;

	public Connection con=null;
	public RaportClienti() throws ClassNotFoundException, SQLException {
		initialize();
		Class.forName("com.mysql.jdbc.Driver");
		con = DriverManager.getConnection("jdbc:mysql://localhost:3306/manage","root","Anonimus1.");
	}
	public void  UpdateTableRapC() throws SQLException{
		
		String query="select * from client";
		PreparedStatement st=con.prepareStatement(query);
		ResultSet rs=st.executeQuery();
		table.setModel(DbUtils.resultSetToTableModel(rs));
	st.close();
	rs.close();
}
	public void populateTable(){
		try {
			String query="select * from client";
			PreparedStatement st=con.prepareStatement(query);
			ResultSet rs=st.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(rs));
			st.close();
			rs.close();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	private void initialize() {
		frmRaportClineti = new JFrame();
		frmRaportClineti.setTitle("Raport Clineti");
		frmRaportClineti.setBounds(100, 100, 450, 449);
		frmRaportClineti.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmRaportClineti.getContentPane().setLayout(null);
		
		JButton button = new JButton("Close");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frmRaportClineti.setVisible(false);
			}
		});
		button.setBounds(116, 390, 200, 16);
		frmRaportClineti.getContentPane().add(button);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 414, 370);
		frmRaportClineti.getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
	}

	public void setVisible(boolean b) {
		frmRaportClineti.setVisible(true);
		
	}

	public void setVisiblee(boolean b) {
		frmRaportClineti.setVisible(true);
		
	}
}
