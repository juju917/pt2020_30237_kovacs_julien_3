package presentation;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class YesNo {

	private JFrame frame;
	public YesNo() {
		initialize();
	}
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 376, 118);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblAreYouSure = new JLabel("Are you sure you want to log out?");
		lblAreYouSure.setBounds(97, 10, 193, 31);
		frame.getContentPane().add(lblAreYouSure);
		
		JButton btnNewButton = new JButton("Yes");
		btnNewButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0){
				
				try {
					Psw pss = new Psw();
					pss.setVisible(true);
				} catch (ClassNotFoundException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				frame.setVisible(false);
				
			}
		});
		btnNewButton.setBounds(58, 52, 62, 23);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnNo = new JButton("No");
		btnNo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Man man;
				try {
					man = new Man();
					man.setVisible(true);
				} catch (ClassNotFoundException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				frame.setVisible(false);
			}
		});
		btnNo.setBounds(242, 52, 62, 23);
		frame.getContentPane().add(btnNo);
	}

	public void setVisible(boolean b) {
		frame.setVisible(true);
		
	}
}
